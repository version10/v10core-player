<?php
    $medianetId = (isset($_GET['id']))?$_GET['id']:'6541895';
    $clientId = (isset($_GET['client']))?$_GET['client']:'artvca';
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
    <!-- * Remplacer tous les @@@ de la page par une valeur appropriée * -->

    <meta charset="utf-8"/>
    <meta name="robots" content="index, follow"/>
    <script src="//assets.adobedtm.com/2eda04f28b1fa2bbcd3ec449dfdc174232ed3359/satelliteLib-74306c976638b05ba3f239cf5faa790c1a5b5fda.js"></script>

    <!-- ********** @1 ********** -->
    <!-- * Le titre de la page doit refléter le contenu de la page * -->
    <title>Player V10</title>

    <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1, user-scalable=no"/>

    <meta id="metaWebTrends" name="WT.sp" content="global;@@@5"/>
    <meta name="WT.clic" content=""/>

    <!-- ********** @6 ********** -->
    <link href="http://ici.radio-canada.ca/content/generated/main.min.css?v=1.0.0.26060" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="http://ici.radio-canada.ca/Content/component.DynamicMenu.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="http://ici.radio-canada.ca/Content/component.StickyNav.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="http://ici.radio-canada.ca/Content/component.Menuzone.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="http://ici.radio-canada.ca/Content/component.Footer.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="http://s.radio-canada.ca/_css/modules/modules.barrepartagee.1.0.0.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="http://s.radio-canada.ca/mp/modules.socialbar.1.0.0.css"/>

    <script>
        window.appHost = 'http://ici.radio-canada.ca';
    </script>

    <script src='http://ici.radio-canada.ca/Content/generated/main.min.js'></script>
    <script src='http://ici.radio-canada.ca/Content/generated/app.min.js'></script>

    <script src='//geoip.radio-canada.ca/geoip.js'></script>
    <script src='http://s.radio-canada.ca/_js/modules/modules.pub.manuel.js'></script>
    <script src='http://s.radio-canada.ca/_js/modules/pub.fixnav.js'></script>
    <script type="text/javascript" src="http://s.radio-canada.ca/mp/viafoura.js"></script>
    <script type="text/javascript" src="http://s.radio-canada.ca/mp/modules.socialbar.executer.6.0.js"></script>

    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

    <script src="https://cdn.jsdelivr.net/npm/ismobilejs@0.5.1/dist/isMobile.min.js"></script>

    <script type="text/javascript">
        var rc_clientId = "<?= $clientId ?>";
    </script>
    <script type="text/javascript" src="https://services.radio-canada.ca/media/player/js/prod?version=latest"></script>

    <script type="text/javascript" src="https://player.telequebec.tv/players/latest/js/vodplayer.min.js"></script>

    <style>
        .absolute-decoration{
            position: absolute;
            z-index: 100;
            width: 100px;
            height: 100px;
            background: #60FF22;
            top:10%;
            left:10%;
        }

        .v10core-rcplayer-mod-fixed{
            max-width: 25vw;
            max-height: 25vh;
            top:15px;
            left:15px;
        }

        .v10core-player-fancybox-footer, .v10core-player-fancybox-header{
            height: 50px;
            background: #60FF22;
        }

        .v10core-player-fancybox-left-panel, .v10core-player-fancybox-right-panel{
            width: 50px;
            background: #FF53E0;
        }

    </style>
</head>

<body class="screen-medium">
<script src='http://s.radio-canada.ca/omniture/omni_stats_base.js?version=2014110501'></script>
<div id="hostPubBanner"></div>
<div id="hostNavSticky"></div>
<div id="hostNavigation"></div>
<div id="hostNavigationZone"></div>
<div id="src-container">
    <div id="src-content" class="clearfix" tabindex="0">

        <h1>HTML5</h1>

        <h2>Default no UI</h2>
        <div data-player-html5-default-no-ui></div>
        <h2>Default</h2>
        <div data-player-html5-default2></div>
        <h2>FancyBox</h2>
        <div data-player-html5-fancybox></div>

        <h1>RC</h1>

        <h2>Default no UI</h2>
        <div data-player-rc-default-no-ui></div>
        <h2>Default</h2>
        <div data-player-rc-default2></div>
        <h2>FancyBox</h2>
        <div data-player-rc-fancybox></div>
        <h2>Default with Attr</h2>
        <div data-v10core-player='{"core": {"debugMode": true, "api": "rc"}, "media": {"id":"<?= $medianetId ?>"}, "events": {"PLAY":"onPlayVideo"}}'></div>
        <h2>Default with Attr (Hide)</h2>
        <div style="display: none;" data-div-hide><div data-v10core-player='{"core": {"debugMode": true, "api": "rc"}, "media": {"id":"<?= $medianetId ?>"}, "events": {"PLAY":"onPlayVideo"}}'></div></div>
        <a href="#" data-unhide>show</a>
        <h2>Default with Attr + Ajax</h2>
        <div data-custom-attr='{"core": {"debugMode": true, "api": "rc"}, "media": {"id":"<?= $medianetId ?>"}}'></div>
        <h2>Absolute mod</h2>
        <div style="position: relative;">
            <div data-player-rc-absolute></div>
            <div class="absolute-decoration"></div>
        </div>
        <h2>Fixed mod</h2>
        <div data-player-rc-fixed></div>

        <h1>Youtube</h1>

        <h2>Youtube no UI</h2>
        <div data-player-youtube-no-ui></div>
        <h2>Youtube</h2>
        <div data-player-youtube></div>
        <h2>Youtube FancyBox</h2>
        <div data-player-youtube-fancy></div>

        <h1>Vimeo</h1>

        <h2>Vimeo no UI</h2>
        <div data-player-vimeo-no-ui></div>
        <h2>Vimeo</h2>
        <div data-player-vimeo></div>
        <h2>Vimeo FancyBox</h2>
        <div data-player-vimeo-fancy></div>

        <h1>Iframe</h1>

        <h2>Iframe no UI</h2>
        <div data-player-iframe-no-ui></div>
        <h2>Iframe</h2>
        <div data-player-iframe></div>
        <h2>Iframe FancyBox</h2>
        <div data-player-iframe-fancy></div>

        <h1>Brightcove</h1>

        <h2>Brightcove no UI</h2>
        <div data-player-brightcove-no-ui></div>
        <h2>Brightcove</h2>
        <div data-player-brightcove></div>
        <h2>Brightcove FancyBox</h2>
        <div data-player-brightcove-fancy></div>

        <h1>Telequebec</h1>

        <h2>TQ no UI</h2>
        <div data-player-tq-no-ui></div>
        <h2>TQ</h2>
        <div data-player-tq></div>
        <h2>TQ FancyBox</h2>
        <div data-player-tq-fancy></div>

        <script>if (typeof define != "undefined") define.amd = false;</script>

        <script src="https://code.jquery.com/jquery-3.2.1.min.js"
                integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

        <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>

        <script src="https://www.youtube.com/iframe_api"></script>
        <script src="https://player.vimeo.com/api/player.js"></script>

        <script src="https://use.fontawesome.com/2deb97109c.js"></script>

        <script type="text/javascript" src="dist/Player.min.js?v=5"></script>

        <script>

            function onPlayVideo(rcPlayer){
                console.log("playVideo Attr !", rcPlayer);
            }

            jQuery(document).ready(function ($) {

                $("[data-unhide]").on("click", function(e){
                    e.preventDefault();

                    $("[data-div-hide]").show();
                })

                //Example default no ui
                var vDefaultNoUI = new V10Core.Player({
                    core: {
                        container: "[data-player-html5-default-no-ui]",
                        name: "Player no ui",
                        api:"html5",
                        debugMode: true
                    },
                    html5Params:{
                        autoplay:true
                    },
                    events: {
                        PLAY: function () {
                            console.log("play!");
                        }
                    }
                });

                //Example default
                var vDefault2 = new V10Core.Player({
                    core: {
                        container: "[data-player-html5-default2]",
                        name: "Player default",
                        api:"html5",
                        debugMode: true
                    },
                    html5Params:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    events: {
                        PLAY: function () {
                            console.log("play!");
                        }
                    }
                });

                var vFancybox = new V10Core.Player({
                    core: {
                        container: "[data-player-html5-fancybox]",
                        name: "Player FancyBox",
                        api:"html5",
                        debugMode: true
                    },
                    html5Params:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    events: {
                        PLAY: function () {
                            console.log("play!");
                        }
                    },
                    mod:V10Core.Player.mods.FANCY_BOX
                });

                //Example default no ui
                var rcDefaultNoUI = new V10Core.Player({
                    core: {
                        container: "[data-player-rc-default-no-ui]",
                        name: "Player no ui",
                        api: "rc",
                        debugMode: true
                    },
                    media:{
                      id:"<?= $medianetId ?>"
                    },
                    events: {
                        PLAY: function (rcPlayer) {
                            console.log("play!", rcPlayer);
                        }
                    }
                });

                //Example default
                var rcDefault2 = new V10Core.Player({
                    core: {
                        container: "[data-player-rc-default2]",
                        name: "Player default",
                        api: "rc",
                        debugMode: true
                    },
                    media:{
                        id:"<?= $medianetId ?>"
                    },
                    rcParams:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    events: {
                        PLAY: function (rcPlayer) {
                            console.log("play!", rcPlayer);
                        }
                    }
                });

                //Example fancybox
                var rcFancybox = new V10Core.Player({
                    core: {
                        container: "[data-player-rc-fancybox]",
                        name: "Player FancyBox",
                        api: "rc",
                        debugMode: true
                    },
                    media:{
                        id:"<?= $medianetId ?>"
                    },
                    rcParams:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    events: {
                        PLAY: function (rcPlayer) {
                            console.log("play!", rcPlayer);
                        }
                    },
                    mod:V10Core.Player.mods.FANCY_BOX
                });

                //Example fancybox auto open
                var rcFancybox = new V10Core.Player({
                    core: {
                        name: "Player FancyBox auto open",
                        api: "rc",
                        debugMode: true
                    },
                    media:{
                        id:"<?= $medianetId ?>"
                    },
                    rcParams:{
                        autoplay:false
                    },
                    lightboxParams:{
                        header:{
                            contentType:'url',
                            content:'http://local.radio-canada.ca/v10core-player/content.php'
                        },
                        footer:{
                            className:'v10core-player-fancybox-footer',
                            contentType:'html',
                            content:'<p>my footer</p>'
                        },
                        leftPanel:{
                            className:'v10core-player-fancybox-left-panel',
                            contentType:'html',
                            content:'<p>my right panel</p>'
                        },
                        rightPanel:{
                            className:'v10core-player-fancybox-right-panel',
                            contentType:'html',
                            content:'<p>my left panel</p>'
                        }
                    },
                    mod:V10Core.Player.mods.FANCY_BOX
                });

                //Custom
                V10Core.Player.initPlayerByAttr("data-custom-attr");

                //Example Absolute
                var rcFancybox = new V10Core.Player({
                    core: {
                        container: "[data-player-rc-absolute]",
                        name: "Player Absolute",
                        api: "rc",
                        debugMode: true
                    },
                    media:{
                        id:"<?= $medianetId ?>"
                    },
                    rcParams:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    mod:V10Core.Player.mods.ABSOLUTE
                });

                //Example Fixed
                var rcFancybox = new V10Core.Player({
                    core: {
                        container: "[data-player-rc-fixed]",
                        name: "Player Fixed",
                        api: "rc",
                        debugMode: true
                    },
                    media:{
                        id:"<?= $medianetId ?>"
                    },
                    rcParams:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    mod:V10Core.Player.mods.FIXED
                });

                var vYoutubeNoUI = new V10Core.Player({
                    core: {
                        container: "[data-player-youtube-no-ui]",
                        api: "youtube",
                        name: "Player Youtube",
                        debugMode: true
                    }
                });

                var vYoutube = new V10Core.Player({
                    core: {
                        container: "[data-player-youtube]",
                        api: "youtube",
                        name: "Player Youtube",
                        debugMode: true
                    },
                    youtubeParams:{
                        autoplay:0
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    }
                });

                var vYoutubeFancy = new V10Core.Player({
                    core: {
                        container: "[data-player-youtube-fancy]",
                        api: "youtube",
                        name: "Player Youtube",
                        debugMode: true
                    },
                    youtubeParams:{
                        autoplay:0
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    mod:V10Core.Player.mods.FANCY_BOX
                });

                var vVimeoNoUI = new V10Core.Player({
                    core: {
                        container: "[data-player-vimeo-no-ui]",
                        api: "vimeo",
                        name: "Player Vimeo",
                        debugMode: true
                    }
                });

                var vVimeo = new V10Core.Player({
                    core: {
                        container: "[data-player-vimeo]",
                        api: "vimeo",
                        name: "Player Vimeo",
                        debugMode: true
                    },
                    vimeoParams:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    }
                });

                var vVimeoFancy = new V10Core.Player({
                    core: {
                        container: "[data-player-vimeo-fancy]",
                        api: "vimeo",
                        name: "Player Vimeo",
                        debugMode: true
                    },
                    vimeoParams:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    mod:V10Core.Player.mods.FANCY_BOX
                });

                var vIframeNoUI = new V10Core.Player({
                    core: {
                        container: "[data-player-iframe-no-ui]",
                        api: "iframe",
                        name: "Player Iframe",
                        debugMode: true
                    }
                });

                var vIframe = new V10Core.Player({
                    core: {
                        container: "[data-player-iframe]",
                        api: "iframe",
                        name: "Player Iframe",
                        debugMode: true
                    },
                    iframeParams:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    }
                });

                var vIframeFancy = new V10Core.Player({
                    core: {
                        container: "[data-player-iframe-fancy]",
                        api: "iframe",
                        name: "Player Iframe",
                        debugMode: true
                    },
                    iframeParams:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    mod:V10Core.Player.mods.FANCY_BOX
                });

                var vFancy = new V10Core.Player({
                    core: {
                        container: "[data-player-fancy]",
                        api: "rc",
                        debugMode: true
                    },
                    fancyboxParams:{
                        buttons : [
                            'close',
                            'share'
                        ]
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    mod:V10Core.Player.mods.FANCY_BOX
                });

                var vBrightcoveNoUI = new V10Core.Player({
                    core: {
                        container: "[data-player-brightcove-no-ui]",
                        api: "brightcove",
                        name: "Player Brightcove",
                        debugMode: true
                    }
                });

                var vBrightcove = new V10Core.Player({
                    core: {
                        container: "[data-player-brightcove]",
                        api: "brightcove",
                        name: "Player Brightcove",
                        debugMode: true
                    },
                    brightcoveParams:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    }
                });

                var vBrightcoveFancy = new V10Core.Player({
                    core: {
                        container: "[data-player-brightcove-fancy]",
                        api: "brightcove",
                        name: "Player Brightcove",
                        debugMode: true
                    },
                    brightcoveParams:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    mod:V10Core.Player.mods.FANCY_BOX
                });

                var vTQNoUI = new V10Core.Player({
                    core: {
                        container: "[data-player-tq-no-ui]",
                        api: "tq",
                        name: "Player TQ",
                        debugMode: true
                    }
                });

                var vTQ = new V10Core.Player({
                    core: {
                        container: "[data-player-tq]",
                        api: "tq",
                        name: "Player TQ",
                        debugMode: true
                    },
                    tqParams:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    }
                });

                var vTQFancy = new V10Core.Player({
                    core: {
                        container: "[data-player-tq-fancy]",
                        api: "tq",
                        name: "Player TQ",
                        debugMode: true
                    },
                    tqParams:{
                        autoplay:false
                    },
                    ui: {
                        poster: "http://via.placeholder.com/990x556"
                    },
                    mod:V10Core.Player.mods.FANCY_BOX
                });
            });
        </script>
    </div>
</div>

<div id="hostFooter"></div>

<script>
    /*
     Sections disponibles
     --------------
     abitibi-temiscamingue
     acadie
     alberta
     colombie-britannique-et-yukon
     est-du-quebec
     estrie
     grandmontreal
     manitoba
     mauricie
     ontario
     ottawa-gatineau
     quebec
     saguenay-lac-saint-jean
     saskatchewan

     info
     premiere
     tele
     */

    var section = 'tele';

    function loadAdvertising() {
        bunkerRequire(['underscore', 'services/componentsService', 'externals/components/adsmanager/main'], function (_, cpSvc, adsmanager) {
            adsmanager.init();
            adsmanager.processInit();
        });
    }

    bunkerRequire(['jquery', 'services/componentsService', 'underscore'], function (jq, cpSvc, _) {
        var navAsync = cpSvc.load('dynamicmenu', {hideleaderboard: true}, document.getElementById('hostNavigation'));
        var footerAsync = cpSvc.load('footer', {}, document.getElementById('hostFooter'));
        var stickyNavAsync = cpSvc.load('StickyNav', {
            itemSelector: '#hostNavigation',
            url: section
        }, document.getElementById('hostNavSticky'));

        var bannerAsync = cpSvc.load('banner', {}, document.getElementById('hostPubBanner'));

        if (section === 'tele') {
            var navZoneTeleASync = cpSvc.load('menuZone', {}, document.getElementById('hostNavigationZone'));
        }


        /* Exemple d'injection d'un bigbox */
        var bigBoxAsync = cpSvc.load('bigbox', {external: true}, document.getElementById('hostPubBigBox'));

        /* Vous pouvez utiliser les paramètres facultatifs suivants:

         showOnlyWhenView (Valeur par défaut: false): Charger la publicité seulement lorsqu'elle apparaît dans le visuel de l'usager.
         hideOnMobile (Valeur par défaut: false): Ne pas charger la publicité sur un téléphone.
         hideOnTabletMd (Valeur par défaut: false): Ne pas charger la publicité sur une tabelette en format portail.
         hideOnTabletLg (Valeur par défaut: false): Ne pas charger la publicité sur une tablette en format paysage.
         hideOnDesktop (Valeur par défaut: false): Ne pas charger la publicité sur un ordinateur.
         hidePubLabel (Valeur par défaut: false): Ne pas afficher la mention publicité. (Elle est obligatoire, il faut que vous la gérer manuellement si vous retirer celà. C'est une règle d'accèssiblité.)
         */

        jq.when(navAsync, footerAsync, stickyNavAsync, bannerAsync, bigBoxAsync).done(function (nav, footer, sticky, banner, bigbox) {
            // Fournir un délai pour être certain de ne pas tenter de charger les publicités avant que les fonctions init de chaque composant ait été appelé.
            _.delay(loadAdvertising, 500);
        });
    });
</script>
<script type="text/javascript">_satellite.pageBottom();</script>
</body>

</html>
