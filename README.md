# Player #
![Release](https://img.shields.io/badge/release-v4.0.0-green.svg)

The aim of this project is to provide a easy way to use the player of Radio-Canada.

## Features

* Easy to use
* Super lightweight
* 16x9 auto size
* Custom poster + button play
* Control all players in the same time
* More methods (Global and local)
* Better way integration (JS or Attr)
* Easy Events Callback
* Debug Mode
* Custom Name
* Mods (Absolute, FancyBox, FIXED)
* Add Global String events on Player by Attr
* All events return the Player
* API (Html5 / RC / Youtube / Vimeo / Iframe / Brightcove / TeleQuebec)
* Custom Fancybox layout
* Custom Lightbox layout (header / footer / right panel / left panel)
* Compatible accessibility

## TO DO

# Dependencies

* jQuery
* isMobile
* FancyBox 3.2.5 (*optional - only for FancyBox mod*)
* Radio-Canada API (*optional - only for RC api*)
* Youtube API (*optional - only for Youtube api*)
* Vimeo API (*optional - only for Vimeo api*)
* Brightcove API (*auto install if used - only for Brightcove api*)
* TeleQuebec (*optional - only for TeleQuebec api*)
* [Console Audio-Video RC](https://docs.google.com/document/d/1KUfE9DU4o6Wa4pH_QMVhYb1OPxJLQJmursEyKUfB3OU/edit)

# Installing

* `bower install https://bitbucket.org/version10/v10core-player.git --save`
* `<script type="text/javascript" src="https://services.radio-canada.ca/media/player/client/{idClient}"></script>`
* `<script src="https://www.youtube.com/iframe_api"></script>`
* `<script src="https://player.vimeo.com/api/player.js"></script>`
* `<script src="//remote.vroptimal-3dx-assets.com/scripts/vroptimal.js"></script>`
* `<script type="text/javascript" src="https://player.telequebec.tv/players/latest/js/vodplayer.min.js"></script>`

## Using

```javascript
    var player = new V10Core.Player({OPTIONS});
```

## Options.core

Core | Type | Default | Description
------------ | ------ | --------- | -------------
container | String | `null` | container name selector
name | String | `''` | Custom name (optional)
api | String | `'html5'` | API (html5 / rc / youtube / vimeo / iframe / brightcove / tq)
debugMode | Boolean | `false` | Active Debug Mode (Logs Help)

## Options.media

Media | Type | Default | Description
------------ | ------ | --------- | -------------
id | String | `''` | id media of the api

- html5: url
- rc: mediaId
- youtube: id
- vimeo: id
- iframe: content html / iframe / url
- brightcove: id
- tq: id

## Options.ui

Media | Type | Default | Description
------------ | ------ | --------- | -------------
poster | String | `''` | poster image
btnPlay | String | `'<i class='fa fa-play-circle-o fa-5x' aria-hidden='true'></i>'` | btn play
width | String | `'100%'` | width of the player
height | String | `'100%'` | height of the player

## Options.html5Params

Video Params | Type | Default | Description
------------ | ------ | --------- | -------------
autoplay | Boolean | `false` | Auto play the player on init
controls | Boolean | `true` | Show the control
muted | Boolean | `false` | Specifies that the audio output of the video should be muted
loop | Boolean | `false` | Specifies that the video will start over again, every time it is finished

## Options.rcParams

RC Params | Type | Default | Description
------------ | ------ | --------- | -------------
appCode | String | `'medianet'` | app code
autoPlay | Boolean | `false` | Auto play the player on init
urlTeaser | String | `null` | Poster image
canExtract | Boolean | `false` | Can extract the video
time | Number | `0` | Start time to seek
infoBar | Boolean | `false` | Show the info bar
share | Boolean | `false` | Can share the video
multiInstance | Boolean | `false` | Can play multiple video in the same time
lang | String | `'fr'` | Default language,
audioMode | Boolean | `false` | Is audio only
controlBar | Boolean | `true` | Show the control bar
muted | Boolean | `false` | Try mute the video

## Options.youtubeParams

Video Params | Type | Default | Description
------------ | ------ | --------- | -------------
autoplay | Boolean | `0` | Auto play the player on init

## Options.vimeoParams

Video Params | Type | Default | Description
------------ | ------ | --------- | -------------
autoplay | Boolean | `false` | Auto play the player on init

## Options.iframeParams

Video Params | Type | Default | Description
------------ | ------ | --------- | -------------

## Options.brightcoveParams

Video Params | Type | Default | Description
------------ | ------ | --------- | -------------
accountId | String | `"2890187620001"` | Brightcove account ID
playerId | String | `"r1fOwnEZW7"` | Brightcove player ID
autoplay | Boolean | `false` | Auto play the player on init
controls | Boolean | `true` | Show the control

## Options.tqParams

Video Params | Type | Default | Description
------------ | ------ | --------- | -------------
autoplay | Boolean | `false` | Auto play the player on init

## Options.fancyboxParams

Video Params | Type | Default | Description
------------ | ------ | --------- | -------------
closeExisting | Boolean | `true` |
closeButton | Boolean | `true` |
l10n | Object | `{FR textes}` |
on.reveal | Function | `built in function` |
on.close | Function | `built in function` |
on.done | Function | `built in function` |


## Options.events

Events | Type | Default | Description
------------ | ------ | --------- | ----------------
PLAYER_UI_READY | Function | `null` | Call After the UI ready (poster + buttons)
PLAYER_READY | Function | `null` | Call After the player is ready and events ready
BEGIN | Function | `null` | Video begin *radio-canada event*
END | Function | `null` | Video ended
START | Function | `null` | Video start *radio-canada event*
FIRST_PLAY | Function | `null` | Call After the first play
PLAY | Function | `null` | On Play
PAUSE | Function | `null` | On Pause
EXTRACT | Function | `null` | *radio-canada event*
SEEKED | Function | `null` | On seek to time
CURRENT_TIME_CHANGE | Function | `null` | *radio-canada event*
PRESENCE | Function | `null` | *radio-canada event*
AD_STARTED | Function | `null` | *radio-canada event*
AD_PAUSE | Function | `null` | *radio-canada event*
AD_COMPLETE | Function | `null` | *radio-canada event*
NEXT | Function | `null` | *radio-canada event*
PREVIOUS | Function | `null` | *radio-canada event*
SHARE | Function | `null` | *radio-canada event*
VOLUME_CHANGE | Function | `null` | On volume change
MUTE | Function | `null` | On mute
UNMUTE | Function | `null` | On unmute
ENTER_FULL_SCREEN | Function | `null` | *radio-canada event*
EXIT_FULL_SCREEN | Function | `null` | *radio-canada event*
COMPLETE | Function | `null` | *radio-canada event*
ERROR | Function | `null` | On Error
META_LOADED | Function | `null` | *radio-canada event*
NEXT_CHAPTER | Function | `null` | *radio-canada event*
END_CHAPTER | Function | `null` | *radio-canada event*

## Options.mod

Mod | value | Description
------------ | ---------
DEFAULT | `default` | NO Mod (Default player)
ABSOLUTE | `absolute` | Show the player on top while playing
FANCY_BOX | `fancybox` | Show the player on FancyBox3
FIXED | `fixed` | Show the player on top and fallow

## Example HTML5 Player (Default)

```javascript
    jQuery(document).ready(function($) {

        var html5 = new V10Core.Player({
            core: {
                container: "[data-player-container]",
                name: "Player default",
                api:"html5",
                debugMode: true
            },
            html5Params:{
                autoplay:true
            },
            ui: {
                poster: "http://via.placeholder.com/990x556"
            }
        });

    });
```

## Example RC Player (Default + PLAY event)

```javascript
    jQuery(document).ready(function($) {

        var rc = new V10Core.Player({
            core:{
                container:"[data-player-container]",
                api:"rc"
            },
            media:{
                 id:"7561489"
            },
            events: {
                PLAY: function (player) {
                    console.log("play!");
                }
            }
        });

    });
```

## Example RC Player (Custom UI)

```javascript
   jQuery(document).ready(function($) {

       var rc = new V10Core.Player({
           core:{
               container:"[data-player-container]",
               api:"rc"
           },
           media:{
                id:"7561489"
           },
           ui: {
               poster: "http://via.placeholder.com/990x556"
           }
       });

   });
```

## Example RC Player (Custom UI + Fancybox)

```javascript
    jQuery(document).ready(function($) {

        var rc = new V10Core.Player({
            core:{
                container:"[data-player-container]",
                api:"rc"
            },
            media:{
                 id:"7561489"
            },
            rcParams:{
                autoPlay:true
            },
            ui: {
                poster: "http://via.placeholder.com/990x556"
            },
            mod:V10Core.Player.mods.FANCY_BOX
        });

    });
```

## Example RC Player (Open Fancybox on page load)

```javascript
    jQuery(document).ready(function($) {

        var rc = new V10Core.Player({
            core:{
                container:"[data-player-container]",
                api:"rc"
            },
            media:{
                 id:"7561489"
            },
            rcParams:{
                autoPlay:true
            },
            mod:V10Core.Player.mods.FANCY_BOX
        });

    });
```

## Example RC Player (Absolute)

```javascript
       jQuery(document).ready(function($) {

           var rc = new V10Core.Player({
               core:{
                   container:"[data-player-container]",
                   api:"rc"
               },
               media:{
                    id:"7561489"
               },
               rcParams:{
                   autoPlay:true
               },
               ui: {
                   poster: "http://via.placeholder.com/990x556"
               },
               mod:V10Core.Player.mods.ABSOLUTE
           });

       });
```

## Example RC Player (Fixed)

```javascript
    jQuery(document).ready(function($) {

        var rc = new V10Core.Player({
            core:{
                container:"[data-player-container]",
                api:"rc"
            },
            media:{
                 id:"7561489"
            },
            rcParams:{
                autoPlay:true
            },
            ui: {
                poster: "http://via.placeholder.com/990x556"
            },
            mod:V10Core.Player.mods.FIXED
        });

    });
```

```CSS
    .v10core-player-mod-fixed{
        max-width: 25vw;
        max-height: 25vh;
        top:15px;
        left:15px;
    }
```

## Example Youtube Player

```javascript
    jQuery(document).ready(function($) {

        var vYoutube = new V10Core.Player({
            core: {
                container: "[data-player-container]",
                api: "youtube",
                name: "Player Youtube",
                debugMode: true
            },
            youtubeParams:{
                autoplay:1
            },
            ui: {
                poster: "http://via.placeholder.com/990x556"
            }
        });

    });
```

## Example Vimeo Player

```javascript
    jQuery(document).ready(function($) {

        var vVimeo = new V10Core.Player({
            core: {
                container: "[data-player-container]",
                api: "vimeo",
                name: "Player Youtube",
                debugMode: true
            },
            vimeoParams:{
                autoplay:true
            },
            ui: {
                poster: "http://via.placeholder.com/990x556"
            }
        });

    });
```

## Example Iframe Player

```javascript
    jQuery(document).ready(function($) {

        var vIframe = new V10Core.Player({
            core: {
                container: "[data-player-iframe]",
                api: "iframe",
                name: "Player Iframe",
                debugMode: true
            },
            ui: {
                poster: "http://via.placeholder.com/990x556"
            }
        });

    });
```

## Example Brightcove Player

```javascript
    jQuery(document).ready(function($) {

        var vBrightcove = new V10Core.Player({
            core: {
                container: "[data-player-container]",
                api: "brightcove",
                name: "Player Brightcove",
                debugMode: true
            },
            brightcoveParams:{
                accountId:"2890187620001",
                playerId:"r1fOwnEZW7"
                autoplay:true
            },
            ui: {
                poster: "http://via.placeholder.com/990x556"
            }
        });

    });
```

## Example TQ Player (Custom UI)

```javascript
   jQuery(document).ready(function($) {

       var rc = new V10Core.Player({
           core:{
               container:"[data-player-container]",
               api:"tq"
           },
           media:{
                id:"50206"
           },
           ui: {
               poster: "http://via.placeholder.com/990x556"
           }
       });

   });
```

## Example Player (HTML Attr method)

```HTML
    <div data-v10core-player='{"core":{"api":"html5"}, "media": {"id":"7561489"}, "mod":"fancybox"}'></div>
```

## Example Player (HTML Custom Attr method + Ajax)

```HTML
    <div data-custom-attr='{"core":{"api":"html5"}, "media": {"id":"7561489"}}'></div>
```

```javascript
    jQuery(document).ready(function($) {
        V10Core.Player.initPlayerByAttr("data-custom-attr");
    });
```

## Example Player (HTML Attr method + Ajax)

```HTML
    <div data-v10core-player='{"core":{"api":"rc"}, "media": {"id":"7561489"}, "events": {"PLAY":"onPlayVideo"}}'></div>
```

```javascript
    jQuery(document).ready(function($) {
        V10Core.Player.initPlayerByAttr(V10Core.Player.playerAttribute);
    });

    function onPlayVideo(player){

    }
```


## Global Methods

*Pause*

Pause all videos

```
    V10Core.Player.pause();
```

*Stop*

Stop all videos

```
    V10Core.Player.stop();
```

*players*

Return the list of Player

```
    V10Core.Player.players
```

*count*

Return the number of Player

```
    V10Core.Player.count
```

*initPlayerByAttr*

Create new players by the attribute

```
    V10Core.Player.initPlayerByAttr(V10Core.Player.PlayerAttribute);

    V10Core.Player.initPlayerByAttr("data-custom-attr");
```



## Local Methods

*Play*

Play the video

```
    player.play();
```

*Pause*

Pause the video

```
    player.pause();
```

*Stop*

Stop the video

```
    player.stop();
```

*Change*

Change the video id (id, CUSTOM_RC_PARAMS)

```
    player.change("7561489", {autoPlay:true});
```

### Credits ###

- Michaël Chartrand (SirKnightDragoon)
