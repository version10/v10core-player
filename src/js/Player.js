if(typeof V10Core === "undefined") V10Core = {};

V10Core.Player = function(options){
    var _this = this;

    //Init Options
    this.options = {};

    //Player Core options
    this.options.core = {
        name:"",
        api:"html5",
        container:null,
        debugMode:false
    };

    //Media options
    this.options.media = {
        id:""
    }

    //Custom UI option
    this.options.ui = {
        poster:"",
        btnPlay:"<i class='fa fa-play-circle-o fa-5x' aria-hidden='true'></i>",
        width: "100%",
        height: "100%"
    };

    this.options.html5Params = {
        autoplay: false,
        controls: true,
        muted: false,
        loop: false
    };

    this.options.rcParams = {
        clientId:rc_clientId,
        appCode:"medianet",
        autoplay: false,
        meta:{teaserUrl:null},
        canExtract:false,
        time:0,
        infoBar:false,
        share:false,
        multiInstance:false,
        lang: "fr",
        audioMode:false,
        controlBar:true,
        muted: true
    };

    this.options.youtubeParams = {
        autoplay: 0
    };

    this.options.vimeoParams = {
        autoplay: false
    };

    this.options.iframeParams = {

    };

    this.options.brightcoveParams = {
        accountId:"1752604059001",
        playerId:"rJtrO8EKW",
        autoplay: false,
        controls: true
    };

    this.options.tqParams = {
        autoplay: false
    };

    this.options.fancyboxParams = {
        on:{},
        closeButton:true,
        closeExisting:true,
        l10n : {
            PANUP:"Déplacer vers le haut",
            PANDOWN:"Déplacer vers le bas",
            PANLEFT:"Déplacer vers la gauche",
            PANRIGHT:"Déplacer vers la droite",
            ZOOMIN:"Zoom avant",
            ZOOMOUT:"Zoom arrière",
            TOGGLEZOOM:"Basculer le niveau de zoom",
            TOGGLE1TO1:"Basculer le niveau de zoom",
            ITERATEZOOM:"Basculer le niveau de zoom",
            ROTATECCW:"Tourner dans le sens antihoraire",
            ROTATECW:"Le sens des aiguilles d'une montre",
            FLIPX:"Retourner horizontalement",
            FLIPY:"Retourner verticalement",
            FITX:"Ajuster horizontalement",
            FITY:"Ajuster verticalement",
            RESET:"Réinitialiser",
            TOGGLEFS:"Basculer en plein écran",
            CLOSE:"Fermer",
            NEXT:"Suivant",
            PREV:"Précédent",
            MODAL:"Vous pouvez fermer ce contenu modal avec la touche ESC",
            ERROR:"Quelque chose s'est mal passé," + " veuillez réessayer plus tard",
            IMAGE_ERROR:"Image introuvable",
            ELEMENT_NOT_FOUND:"Élément HTML introuvable",
            AJAX_NOT_FOUND:"Erreur lors du chargement d'AJAX : introuvable",
            AJAX_FORBIDDEN:"Erreur lors du chargement d'AJAX : Interdit",
            IFRAME_ERROR:"Erreur lors du chargement de la page",
            TOGGLE_ZOOM:"Basculer le niveau de zoom",
            TOGGLE_THUMBS:"Basculer les vignettes",
            TOGGLE_SLIDESHOW:"Basculer le diaporama",
            TOGGLE_FULLSCREEN:"Basculer en mode plein écran",
            DOWNLOAD:"Télécharger"
        }
    };

    //Events options callback
    this.options.events = {
        PLAYER_UI_READY:null,
        PLAYER_READY:null,
        BEGIN:null,
        END:null,
        UI_PLAY:null,
        FIRST_PLAY:null,
        START:null,
        PLAY:null,
        PAUSE:null,
        SEEKED :null,
        CURRENT_TIME_CHANGE:null,
        AD_STARTED:null,
        AD_PAUSE:null,
        AD_COMPLETE:null,
        VOLUME_CHANGE:null,
        MUTE:null,
        UNMUTE:null,
        ENTER_FULL_SCREEN:null,
        EXIT_FULL_SCREEN:null,
        COMPLETE:null,
        ERROR:null,
        META_LOADED:null
    }

    //Mod options
    this.options.mod = V10Core.Player.mods.DEFAULT;

    //Add all user options in the options group
    for (key in options.core) {
        this.options.core[key] = options.core[key];
    }

    switch (this.options.core.api){
        case V10Core.Player.apis.HTML5:
            this.options.media.id = "http://www.w3schools.com/html/mov_bbb.mp4";
            break;
        case V10Core.Player.apis.RC:
            this.options.media.id = "6541895";

            //Add fix RC styles
            if($("[data-rc-style]").length == 0) {
                var rcStyle = "";

                rcStyle += ".rcplayer-bottom-controller--top .rcplayer-seekbar-active .rcplayer-seekbar-container input[type='range'] {";
                rcStyle += "overflow: hidden!important;";
                rcStyle += "}";

                rcStyle += ".rcplayer-big-play-btn-container .rcplayer-big-btn-play {";
                rcStyle += "border-radius: 50% !important;";
                rcStyle += "}";

                $("head").append("<style data-rc-style>" + rcStyle + "</style>");
            }
            break;
        case V10Core.Player.apis.YOUTUBE:
            this.options.media.id = "MLpWrANjFbI";
            break;
        case V10Core.Player.apis.VIMEO:
            this.options.media.id = "280293143";
            break;
        case V10Core.Player.apis.IFRAME:
            this.options.media.id = "<iframe frameborder=\"0\" width=\"480\" height=\"270\" src=\"//www.dailymotion.com/embed/video/x6tqhqb\" allowfullscreen=\"\" allow=\"autoplay\"></iframe>";
            break;
        case V10Core.Player.apis.BRIGHTCOVE:
            this.options.media.id = "5550679964001";
            break;
        case V10Core.Player.apis.TQ:
            this.options.media.id = "50206";
            break;
    }

    for (key in options.media) {
        this.options.media[key] = options.media[key];
    }

    for (key in options.ui) {
        this.options.ui[key] = options.ui[key];
    }

    for (key in options.html5Params) {
        this.options.html5Params[key] = options.html5Params[key];
    }

    for (key in options.rcParams) {
        this.options.rcParams[key] = options.rcParams[key];
    }

    for (key in options.youtubeParams) {
        this.options.youtubeParams[key] = options.youtubeParams[key];
    }

    for (key in options.vimeoParams) {
        this.options.vimeoParams[key] = options.vimeoParams[key];
    }

    for (key in options.iframeParams) {
        this.options.iframeParams[key] = options.iframeParams[key];
    }

    for (key in options.brightcoveParams) {
        this.options.brightcoveParams[key] = options.brightcoveParams[key];
    }

    for (key in options.tqParams) {
        this.options.tqParams[key] = options.tqParams[key];
    }

    for (key in options.fancyboxParams) {
        this.options.fancyboxParams[key] = options.fancyboxParams[key];
    }

    this.options.rcParams.width =  this.options.ui.width;
    this.options.rcParams.height =  this.options.ui.height;

    this.options.vimeoParams.width =  this.options.ui.width;
    this.options.vimeoParams.height =  this.options.ui.height;

    for (key in options.events) {
        this.options.events[key] = options.events[key];

        if(typeof options.events[key] == "string"){
            this.options.events[key] = window[options.events[key]];
        }
    }

    //Mod options
    if(options.mod) this.options.mod = options.mod;

    //Add the player in the list
    V10Core.Player.players.push(this);
    V10Core.Player.count++;

    //Create UID of the player
    this.uid = "v10core-player-" + V10Core.Player.count;

    //Set default name
    if(this.options.core.name == "") this.options.core.name = this.uid;

    this.debugLog("Player :: " + this.options.core.name + " :: created");

    //Properties
    this.firstPlay = false;
    this.isAds = false;

    //Init player ui
    this.initUI();

    this.initDelay();
}

//Function initDelay
// -----------------------------------------------------
// Init Delay
V10Core.Player.prototype.initDelay = function(){
    var _this = this;

    if($(this.options.core.container).parents(":hidden").length == 0){
        //Init the player by mod selected
        if((this.options.ui.poster == ""
            || this.options.html5Params.autoplay
            || this.options.rcParams.autoplay
            || this.options.youtubeParams.autoplay
            || this.options.vimeoParams.autoplay
            || this.options.brightcoveParams.autoplay
            || this.options.tqParams.autoplay
            || isMobile.any)
            && this.options.mod == V10Core.Player.mods.DEFAULT
        ){
            switch (this.options.core.api){
                case V10Core.Player.apis.HTML5:
                    this.onUIEventPlay(null);
                    break;
                case V10Core.Player.apis.RC:
                    this.onUIEventPlay(null);
                    break;
                case V10Core.Player.apis.YOUTUBE:
                    if(V10Core.Player.YouTubeIframeAPIReady){
                        this.onUIEventPlay(null);
                    }else{
                        this.intervalYouTubeIframeAPIReady = setInterval(function(){
                            if(V10Core.Player.YouTubeIframeAPIReady){
                                _this.onUIEventPlay(null);
                                clearInterval(_this.intervalYouTubeIframeAPIReady);
                            }
                        }, 500);
                    }
                    break;
                case V10Core.Player.apis.VIMEO:
                    this.onUIEventPlay(null);
                    break;
                case V10Core.Player.apis.IFRAME:
                    this.onUIEventPlay(null);
                    break;
                case V10Core.Player.apis.BRIGHTCOVE:
                    this.onUIEventPlay(null);
                    break;
                case V10Core.Player.apis.TQ:
                    this.onUIEventPlay(null);
                    break;
            }
        }
    }else{
        setTimeout(function(){
            _this.initDelay()
        }, 1000);
    }
};

//Function initUI
// -----------------------------------------------------
// Init the UI of the player
V10Core.Player.prototype.initUI = function(){
    //Insert the uid in the core container
    if(this.options.core.container != null){

        $("head").append("<style type='text/css'>.v10core-player-ui-player iframe, .v10core-player-mod-fancybox iframe, .v10core-player-mod-absolute iframe, .v10core-player-mod-fixed iframe{width:100%; height:100%;}</style>");

        //CSS 16x9 Box
        $(this.options.core.container).css({
            position: "relative",
            display: "block",
            height: 0,
            padding: 0,
            overflow: "hidden",
            paddingBottom: "56.25%"
        });

        //Init core ui group
        this.ui = {};

        //Create the player container ui
        this.ui.player = $("<div class='v10core-player-ui-player' "+this.uid+"-player></div>");
        this.ui.player.attr("id", this.uid);

        $(this.ui.player).css({
            position: "absolute",
            display: "block",
            width: "100%",
            height: "100%"
        });

        $(this.options.core.container).append(this.ui.player);

        //Add poster image
        if(this.options.ui.poster != ""){

            this.ui.poster = $("<a href='#' class='v10core-player-ui-poster' "+this.uid+"-ui-poster><span style='display: none;'>Écouter la vidéo</span></a>");

            this.ui.poster.css({
                position: "absolute",
                top:0,
                left:0,
                display: "block",
                width: "100%",
                height: "100%",
                backgroundImage: "url('"+this.options.ui.poster+"')",
                backgroundRepeat:"no-repeat",
                backgroundSize:"cover",
                zIndex:2,
                cursor:"pointer"
            });

            $(this.options.core.container).append(this.ui.poster);

            if(this.options.ui.btnPlay != ""){
                this.ui.btnPlay = $(this.options.ui.btnPlay);
                this.ui.btnPlay.attr(this.uid+"-ui-btn-play", "");
                this.ui.btnPlay.addClass("v10core-player-ui-btn-play");

                this.ui.btnPlay.css({
                    position:"absolute",
                    top: "50%",
                    left: "50%",
                    webkitTransform: "translate(-50%,-50%)",
                    transform: "translate(-50%,-50%)",
                    zIndex:3,
                    pointerEvents: "none"
                });

                $(this.options.core.container).append(this.ui.btnPlay);
            }

            this.ui.poster.on("click", this.onUIEventPlay.bind(this));
        }
    }

    if(this.options.events.PLAYER_UI_READY != null) this.options.events.PLAYER_UI_READY();
}

//Function initPlayerByMod
// -----------------------------------------------------
// Init the player by mod
V10Core.Player.prototype.initPlayerByMod = function(e){
    switch(this.options.mod){
        case V10Core.Player.mods.DEFAULT:
            this.initDefaultPlayer(e);
            break;
        case V10Core.Player.mods.FANCY_BOX:
            this.initFancyboxPlayer(e);
            break;
        case V10Core.Player.mods.FIXED:
            this.initFixedPlayer(e);
            break;
        case V10Core.Player.mods.ABSOLUTE:
            this.initAbsolutePlayer(e);
            break;
    }
}

//Function initPlayerByAPI
// -----------------------------------------------------
// Init the player by api
V10Core.Player.prototype.initPlayerByAPI = function(uName, e){
    var _this = this;

    switch (this.options.core.api){
        case V10Core.Player.apis.HTML5:
            this.player = $('<video />', {
                id: this.uid+"-video",
                src: this.options.media.id,
                type: 'video/mp4',
                controls: this.options.html5Params.controls,
                autoplay: e != null ? true : this.options.html5Params.autoplay,
                muted: isMobile.any && (e != null || this.options.html5Params.autoplay == true) ? true : this.options.html5Params.muted,
                loop: this.options.html5Params.loop
            });

            if(isMobile.any && this.options.ui.poster != '') this.player.attr('poster',this.options.ui.poster);
            this.player.attr('playsinline','');

            this.player.css({
                width:this.options.ui.width,
                height:this.options.ui.height
            });

            this.player.appendTo($("#"+uName));

            this.eventReady();
            break;
        case V10Core.Player.apis.RC:
            if(this.options.rcParams.muted){
                localStorage.setItem('RC_PLAYER_volume', '{"value":0,"type":"number"}');
            }

            this.options.rcParams.autoplay = e != null ? true : this.options.rcParams.autoplay;
            if(isMobile.any && this.options.ui.poster != '') this.options.rcParams.meta.teaserUrl = this.options.ui.poster;


            this.player = RadioCanadaPlayer.initPlayerJs(uName,
                this.options.media.id,
                this.options.rcParams.appCode,
                this.options.rcParams
            );

            this.player.events.on(this.player.events.names.READY, this.eventReady.bind(this));
            break;
        case V10Core.Player.apis.YOUTUBE:
            this.options.youtubeParams.autoplay = e != null ? 1 : this.options.youtubeParams.autoplay;

            this.player = new YT.Player(uName, {
                height: this.options.ui.width,
                width: this.options.ui.height,
                playerVars:this.options.youtubeParams,
                videoId: this.options.media.id
            });
            this.eventReady();
            break;
        case V10Core.Player.apis.VIMEO:
            this.options.vimeoParams.id = this.options.media.id;

            this.options.vimeoParams.autoplay = e != null ? true : this.options.vimeoParams.autoplay;

            this.player = new Vimeo.Player(uName, this.options.vimeoParams);
            this.eventReady();
            break;
        case V10Core.Player.apis.IFRAME:
            this.player = $("<iframe />", {
                id: this.uid+"-iframe",
                width:this.options.ui.width,
                height:this.options.ui.height,
                frameborder:0,
                allowfullscreen:1
            });

            this.player.appendTo($("#"+uName));

            if(this.options.media.id.indexOf("http") == 0 || this.options.media.id.indexOf("//") == 0){
                this.player.attr("src", this.options.media.id);
            }else{
                var iframe = document.getElementById(this.uid+"-iframe"),
                    iframeWin = iframe.contentWindow || iframe,
                    iframeDoc = iframe.contentDocument || iframeWin.document;

                $(iframeDoc).ready(function (event) {
                    iframeDoc.open();
                    iframeDoc.write(_this.options.media.id);
                    iframeDoc.close();

                    $(iframeDoc).contents().find("iframe").css({
                        width:"100%",
                        height:"100%"
                    });

                    $(iframeDoc).contents().find("body").css({
                        margin:0,
                        padding:0
                    });
                });
            }

            this.player.css({
                width:this.options.ui.width,
                height:this.options.ui.height
            });

            this.eventReady();
            break;
        case V10Core.Player.apis.BRIGHTCOVE:
            this.options.brightcoveParams.autoplay = e != null ? true : this.options.brightcoveParams.autoplay;

            this.player = $('<video />', {
                id: this.uid+"-brightcove",
                "data-video-id":this.options.media.id,
                "data-account":this.options.brightcoveParams.accountId,
                "data-player":this.options.brightcoveParams.playerId,
                "data-embed":"default",
                class:"video-js",
                controls: this.options.brightcoveParams.controls
            });
            this.player.css({
                width:this.options.ui.width,
                height:this.options.ui.height
            });

            this.player.appendTo($("#"+uName));

            var s = document.createElement('script');
            s.src = "//players.brightcove.net/" + this.options.brightcoveParams.accountId + "/" + this.options.brightcoveParams.playerId + "_default/index.min.js";
            document.body.appendChild(s);
            s.onload = function(){
                videojs(_this.uid+"-brightcove").ready(function(){
                    _this.player = this;
                    if (_this.options.brightcoveParams.autoplay) {
                        _this.player.on('loadedmetadata',function() {
                            _this.player.play();
                            _this.onPlayCallback();
                        });
                    }
                    _this.eventReady();
                });
            };
            break;
        case V10Core.Player.apis.TQ:
            this.options.tqParams.autoplay = e != null ? true : this.options.tqParams.autoplay;

            this.player = $('<div />', {
                id: this.uid+"-tq"
            });
            this.player.css({
                width:this.options.ui.width,
                height:this.options.ui.height
            });

            this.player.appendTo($("#"+uName));

            this.player = telequebec.players.VodPlayer($("#"+this.uid+"-tq")[0], { mediaId: this.options.media.id });

            this.eventReady();

            if(this.options.tqParams.autoplay){
                var waitTQ = setInterval(function(){
                    if(_this.player.engine.player != undefined){
                        clearInterval(waitTQ);
                        _this.player.play();
                    }
                }, 50);
            }
            break;
    }
}

//Function initDefaultPlayer
// -----------------------------------------------------
// Init the default player
V10Core.Player.prototype.initDefaultPlayer = function(e){
    this.initPlayerByAPI(this.uid, e);
    this.debugLog("Player :: " + this.options.core.name + " :: initDefaultPlayer");
}

//Function initFancyboxPlayer
// -----------------------------------------------------
// Init the fancybox player
V10Core.Player.prototype.initFancyboxPlayer = function(e){
    var _this = this;

    var fancyboxPlayerCnt = $("<div></div>");

    fancyboxPlayerCnt.addClass('v10core-player-mod-fancybox');
    fancyboxPlayerCnt.attr("id", this.uid+'-fancybox');
    fancyboxPlayerCnt.css({
        display:'none',
        width:"100%",
        height:"100%"
    });

    var onResizeFancyBox = function(fancybox, slide){
        fancyboxPlayerCnt.css({
            width  : '100%',
            height : '100%'
        })

        var ratio = 16 / 9;

        var width  = parseInt(fancyboxPlayerCnt.css("width"));
        var height = parseInt(fancyboxPlayerCnt.css("height"));

        if ( height * ratio > width ) {
            height = width / ratio;
        } else {
            width = height * ratio;
        }

        fancyboxPlayerCnt.css({
            width  : width,
            height : height
        })
    }

    var onReveal = function(fancybox, slide){
        _this.initPlayerByAPI(_this.uid+"-fancybox", e);
    };

    var onClose = function(fancybox, slide){
        _this.stop();
    };

    var onDone = function(fancybox, slide){
        onResizeFancyBox();
    };

    var fancyboxOptions = this.options.fancyboxParams;
    fancyboxOptions.on.reveal = onReveal;
    fancyboxOptions.on.close = onClose;
    fancyboxOptions.on.done = onDone;

    $("body").append(fancyboxPlayerCnt);

    $(window).on("resize", onResizeFancyBox);

    Fancybox.show([{src:'#'+this.uid+'-fancybox'}], fancyboxOptions);

    this.debugLog("Player :: " + this.options.core.name + " :: initFancyboxPlayer");
}

//Function initAbsolutePlayer
// -----------------------------------------------------
// Init the absolute player
V10Core.Player.prototype.initAbsolutePlayer = function(e){
    var _this = this;

    var absolutePlayerCnt = $("<div></div>");
    absolutePlayerCnt.attr("id", this.uid+'-absolute');
    absolutePlayerCnt.addClass('v10core-player-mod-absolute');
    absolutePlayerCnt.css({
        width:$(this.options.core.container).innerWidth(),
        height:$(this.options.core.container).innerHeight(),
        position:"absolute",
        zIndex:"9999999999",
        top:$(this.options.core.container).offset().top,
        left:$(this.options.core.container).offset().left,
        background:"#000"
    });
    $("body").append(absolutePlayerCnt);

    $(window).on("resize", function(){
        absolutePlayerCnt.css({
            width:$(_this.options.core.container).innerWidth(),
            height:$(_this.options.core.container).innerHeight(),
            top:$(_this.options.core.container).offset().top,
            left:$(_this.options.core.container).offset().left
        });
    });

    this.initPlayerByAPI(this.uid+"-absolute", e);

    this.debugLog("Player :: " + this.options.core.name + " :: initAbsolutePlayer");
}

//Function initFixedPlayer
// -----------------------------------------------------
// Init the fixed player
V10Core.Player.prototype.initFixedPlayer = function(e){
    var _this = this;

    var fixedPlayerCnt = $("<div></div>");
    fixedPlayerCnt.attr("id", this.uid+'-fixed');
    fixedPlayerCnt.addClass('v10core-player-mod-fixed');
    fixedPlayerCnt.css({
        position:"fixed",
        zIndex:"9999999999"
    });
    $("body").append(fixedPlayerCnt);

    function updateFixedSize(){
        var ratio = 16 / 9;

        var width  = parseInt(fixedPlayerCnt.css("maxWidth"));
        var height = parseInt(fixedPlayerCnt.css("maxHeight"));

        if ( height * ratio > width ) {
            height = width / ratio;
        } else {
            width = height * ratio;
        }

        fixedPlayerCnt.css({
            width  : width,
            height : height
        })
    }

    $(window).on("resize", updateFixedSize);

    updateFixedSize();

    this.initPlayerByAPI(this.uid+"-fixed", e);

    this.debugLog("Player :: " + this.options.core.name + " :: initAbsolutePlayer");
}

//Method play
// -----------------------------------------------------
// play the player
V10Core.Player.prototype.play = function(){
    if(this.player == undefined) return;

    switch (this.options.core.api){
        case V10Core.Player.apis.HTML5:
            this.player[0].play();
            break;
        case V10Core.Player.apis.RC:
            if(!this.firstPlay) return;
            this.player.api.play();
            break;
        case V10Core.Player.apis.YOUTUBE:
            this.player.playVideo();
            break;
        case V10Core.Player.apis.VIMEO:
            this.player.play();
            break;
        case V10Core.Player.apis.BRIGHTCOVE:
            this.player.play();
            break;
        case V10Core.Player.apis.TQ:
            this.player.play();
            break;
    }

    this.debugLog("Player :: " + this.options.core.name + " :: play");
}

//Method pause
// -----------------------------------------------------
// pause the player
V10Core.Player.prototype.pause = function(){
    if(this.player == undefined) return;

    switch (this.options.core.api){
        case V10Core.Player.apis.HTML5:
            this.player[0].pause();
            break;
        case V10Core.Player.apis.RC:
            if(!this.firstPlay) return;
            this.player.api.pause();
            break;
        case V10Core.Player.apis.YOUTUBE:
            this.player.pauseVideo();
            break;
        case V10Core.Player.apis.VIMEO:
            this.player.pause();
            break;
        case V10Core.Player.apis.BRIGHTCOVE:
            this.player.pause();
            break;
        case V10Core.Player.apis.TQ:
            this.player.pause();
            break;
    }

    this.debugLog("Player :: " + this.options.core.name + " :: pause");
}

//Method stop
// -----------------------------------------------------
// stop the player
V10Core.Player.prototype.stop = function(){
    if(this.player == undefined) return;

    switch (this.options.core.api){
        case V10Core.Player.apis.HTML5:
            this.player[0].pause();
            this.player[0].currentTime = 0;
            break;
        case V10Core.Player.apis.RC:
            if(!this.firstPlay) return;
            this.player.api.pause();
            this.seek(0);
            break;
        case V10Core.Player.apis.YOUTUBE:
            this.player.stopVideo();
            break;
        case V10Core.Player.apis.VIMEO:
            this.player.pause();
            this.player.setCurrentTime(0);
            break;
        case V10Core.Player.apis.BRIGHTCOVE:
            this.player.pause();
            this.player.currentTime(0);
            break;
        case V10Core.Player.apis.TQ:
            this.player.stop();
            break;
    }

    this.debugLog("Player :: " + this.options.core.name + " :: stop");
}

//Method seek
// -----------------------------------------------------
// seek the player
V10Core.Player.prototype.seek = function(pos){
    if(this.player == undefined) return;

    switch (this.options.core.api){
        case V10Core.Player.apis.HTML5:
            this.player[0].currentTime = pos;
            break;
        case V10Core.Player.apis.RC:
            if(!this.firstPlay || this.isAds) return;
            this.player.api.seekTo(pos);
            break;
        case V10Core.Player.apis.YOUTUBE:
            this.player.seekTo(pos);
            break;
        case V10Core.Player.apis.VIMEO:
            this.player.setCurrentTime(pos);
            break;
        case V10Core.Player.apis.BRIGHTCOVE:
            this.player.currentTime(pos);
            break;
        case V10Core.Player.apis.TQ:
            this.player.currentTime(pos);
            break;
    }

    this.debugLog("Player :: " + this.options.core.name + " :: seek", pos);
}

//Method change
// -----------------------------------------------------
// change the media in the player
V10Core.Player.prototype.change = function(id, params){
    if(this.player == undefined) return;

    switch (this.options.core.api){
        case V10Core.Player.apis.HTML5:
            for (key in params) {
                this.options.html5Params[key] = params[key];
            }

            this.options.media.id = id;

            this.player[0].controls = this.options.html5Params.controls;
            this.player[0].autoplay = this.options.html5Params.autoplay;
            this.player[0].src = this.options.media.id;
            this.player[0].currentTime = 0;
            break;
        case V10Core.Player.apis.RC:
            for (key in params) {
                this.options.rcParams[key] = params[key];
            }

            this.options.media.id = id;

            this.player.api.changeMedia(
                this.options.media.id,
                this.options.rcParams.appCode,
                this.options.rcParams
            );
            break;
        case V10Core.Player.apis.YOUTUBE:
            for (key in params) {
                this.options.youtubeParams[key] = params[key];
            }

            this.options.media.id = id;

            this.player.loadVideoById(this.options.media.id);
            break;
        case V10Core.Player.apis.VIMEO:

            this.options.media.id = id;

            this.player.loadVideo(this.options.media.id)
            break;
        case V10Core.Player.apis.TQ:
            this.options.media.id = id;
            this.player = telequebec.players.VodPlayer($("#"+this.uid+"-tq")[0], { mediaId: this.options.media.id });
            break;
        case V10Core.Player.apis.BRIGHTCOVE:
            this.options.media.id = id;
            this.player.src(this.options.media.id);
            break;
    }

    this.debugLog("Player :: " + this.options.core.name + " :: change", id, rcParams);
}

//Function eventReady
// -----------------------------------------------------
// Init all the events of the player
V10Core.Player.prototype.eventReady = function(){

    var _this = this;

    switch (this.options.core.api){
        case V10Core.Player.apis.HTML5:
            this.player[0].addEventListener("playing", this.onStartCallback.bind(this));
            this.player[0].addEventListener("ended", this.onEndCallback.bind(this));
            this.player[0].addEventListener("seeked", this.onSeekedCallback.bind(this));
            this.player[0].addEventListener("play", this.onPlayCallback.bind(this));
            this.player[0].addEventListener("pause", this.onPauseCallback.bind(this));
            this.player[0].addEventListener("timeupdate", this.onCurrentTimeChangeCallback.bind(this));
            this.player[0].addEventListener("error", this.onErrorCallback.bind(this));
            this.player[0].addEventListener("loadedmetadata", this.onMetaLoadedCallback.bind(this));
            this.player[0].addEventListener("volumechange", this.onVolumeChangeCallback.bind(this));

            this.isMuted = this.player[0].muted;

            this.player.bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
                if(document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen){
                    _this.onEnterFullScreenCallback();
                }else{
                    _this.onExitFullScreenCallback();
                }
            });
            break;
        case V10Core.Player.apis.RC:
            this.player.events.on(this.player.events.names.BEGIN, this.onBeginCallback.bind(this));
            this.player.events.on(this.player.events.names.END, this.onEndCallback.bind(this));
            this.player.events.on(this.player.events.names.START, this.onStartCallback.bind(this));
            this.player.events.on(this.player.events.names.PLAY, this.onPlayCallback.bind(this));
            this.player.events.on(this.player.events.names.PAUSE, this.onPauseCallback.bind(this));
            this.player.events.on(this.player.events.names.SEEK_END || this.player.events.names.SEEK_START, this.onSeekedCallback.bind(this));
            this.player.events.on(this.player.events.names.CURRENT_TIME_CHANGE, this.onCurrentTimeChangeCallback.bind(this));
            this.player.events.on(this.player.events.names.AD_STARTED, this.onAdStartedCallback.bind(this));
            this.player.events.on(this.player.events.names.AD_PAUSE, this.onAdPauseCallback.bind(this));
            this.player.events.on(this.player.events.names.AD_COMPLETE, this.onAdCompleteCallback.bind(this));
            this.player.events.on(this.player.events.names.VOLUME_CHANGE, this.onVolumeChangeCallback.bind(this));
            this.player.events.on(this.player.events.names.MUTE, this.onMuteCallback.bind(this));
            this.player.events.on(this.player.events.names.UNMUTE, this.onUnMuteCallback.bind(this));
            this.player.events.on(this.player.events.names.ENTER_FULL_SCREEN, this.onEnterFullScreenCallback.bind(this));
            this.player.events.on(this.player.events.names.EXIT_FULL_SCREEN, this.onExitFullScreenCallback.bind(this));
            this.player.events.on(this.player.events.names.ERROR, this.onErrorCallback.bind(this));
            this.player.events.on(this.player.events.names.META_CHANGED, this.onMetaLoadedCallback.bind(this));

            if(this.options.events.PLAYER_READY != null) this.options.events.PLAYER_READY();

            this.bugFix();
            break;
        case V10Core.Player.apis.YOUTUBE:
            this.player.addEventListener("onReady", this.onPlayerReadyCallback.bind(this));
            this.player.addEventListener("onStateChange", this.onStateChangeYoutubeCallback.bind(this));
            break;
        case V10Core.Player.apis.VIMEO:
            this.player.on("loaded", this.onPlayerReadyCallback.bind(this));
            this.player.on("play", this.onPlayCallback.bind(this));
            this.player.on("pause", this.onPauseCallback.bind(this));
            this.player.on("ended", this.onEndCallback.bind(this));
            this.player.on("volumechange", this.onVolumeChangeCallback.bind(this));
            this.player.on("error", this.onErrorCallback.bind(this));
            break;
        case V10Core.Player.apis.BRIGHTCOVE:
            this.player.on("play", this.onPlayCallback.bind(this));
            this.player.on("pause", this.onPauseCallback.bind(this));
            this.player.on("ended", this.onEndCallback.bind(this));
            break;
        case V10Core.Player.apis.TQ:
            this.player.on('ready', this.onPlayerReadyCallback.bind(this));
            this.player.on('error', function (error) {});
            this.player.on('playStateChanged', function () {});
            this.player.on('mediaComplete', this.onEndCallback.bind(this));
            this.player.on('fullScreenChanged', function (isInFullScreenMode) {});
            break;
    }

    this.debugLog("Player :: " + this.options.core.name + " :: eventReady");
}

//Event onPlayerReadyCallback
// -----------------------------------------------------
V10Core.Player.prototype.onPlayerReadyCallback = function(){
    if(this.options.events.PLAYER_READY != null) this.options.events.PLAYER_READY(this);

    this.debugLog("Player :: " + this.options.core.name + " :: PLAYER_READY");
}

//Event onStateChangeCallback
// -----------------------------------------------------
V10Core.Player.prototype.onStateChangeYoutubeCallback = function(result){

    if(this.options.core.api == "youtube") {
        switch (result.data) {
            case -1:

                break;
            case 0:
                this.onEndCallback()
                break;
            case 1:
                this.onPlayCallback();
                break;
            case 2:
                this.onPauseCallback();
                break;
            case 3:

                break;
            case 5:

                break;
        }
    }
}

//Event onBeginCallback
// -----------------------------------------------------
V10Core.Player.prototype.onBeginCallback = function(){
    if(this.options.events.BEGIN != null) this.options.events.BEGIN(this);
    this.debugLog("Player :: " + this.options.core.name + " :: BEGIN");

    if(!this.firstPlay){
        this.firstPlay = true;

        if(this.options.events.FIRST_PLAY != null) this.options.events.FIRST_PLAY(this);
        this.debugLog("Player :: " + this.options.core.name + " :: FIRST_PLAY");
    }
}

//Event onEndCallback
// -----------------------------------------------------
V10Core.Player.prototype.onEndCallback = function(){
    if(this.options.events.END != null) this.options.events.END(this);
    this.debugLog("Player :: " + this.options.core.name + " :: END");
}

//Event onStartCallback
// -----------------------------------------------------
V10Core.Player.prototype.onStartCallback = function(){
    if(this.options.events.START != null) this.options.events.START(this);
    this.debugLog("Player :: " + this.options.core.name + " :: START");
}

//Event onPlayCallback
// -----------------------------------------------------
V10Core.Player.prototype.onPlayCallback = function(){
    if(this.options.events.PLAY != null) this.options.events.PLAY(this);
    this.debugLog("Player :: " + this.options.core.name + " :: PLAY");

    if(!this.firstPlay){
        this.firstPlay = true;

        if(this.options.events.FIRST_PLAY != null) this.options.events.FIRST_PLAY(this);
        this.debugLog("Player :: " + this.options.core.name + " :: FIRST_PLAY");
    }
}

//Event onPauseCallback
// -----------------------------------------------------
V10Core.Player.prototype.onPauseCallback = function(){
    if(this.options.events.PAUSE != null) this.options.events.PAUSE(this);
    this.debugLog("Player :: " + this.options.core.name + " :: PAUSE");
}

//Event onSeekedCallback
// -----------------------------------------------------
V10Core.Player.prototype.onSeekedCallback = function(seek){
    if(this.options.events.SEEKED != null) this.options.events.SEEKED(this);
    this.debugLog("Player :: " + this.options.core.name + " :: SEEKED", seek);
}

//Event onCurrentTimeChangeCallback
// -----------------------------------------------------
V10Core.Player.prototype.onCurrentTimeChangeCallback = function(){
    if(this.options.events.CURRENT_TIME_CHANGE != null) this.options.events.CURRENT_TIME_CHANGE(this);
    this.debugLog("Player :: " + this.options.core.name + " :: CURRENT_TIME_CHANGE");
}

//Event onAdStartedCallback
// -----------------------------------------------------
V10Core.Player.prototype.onAdStartedCallback = function(){
    if(this.options.events.AD_STARTED != null) this.options.events.AD_STARTED(this);
    this.isAds = true;
    this.debugLog("Player :: " + this.options.core.name + " :: AD_STARTED");

    if(!this.firstPlay){
        this.firstPlay = true;

        if(this.options.events.FIRST_PLAY != null) this.options.events.FIRST_PLAY(this);
        this.debugLog("Player :: " + this.options.core.name + " :: FIRST_PLAY");
    }
}

//Event onAdPauseCallback
// -----------------------------------------------------
V10Core.Player.prototype.onAdPauseCallback = function(){
    if(this.options.events.AD_PAUSE != null) this.options.events.AD_PAUSE(this);
    this.debugLog("Player :: " + this.options.core.name + " :: AD_PAUSE");
}

//Event onAdCompleteCallback
// -----------------------------------------------------
V10Core.Player.prototype.onAdCompleteCallback = function(){
    if(this.options.events.AD_COMPLETE != null) this.options.events.AD_COMPLETE(this);
    this.isAds = false;
    this.debugLog("Player :: " + this.options.core.name + " :: AD_COMPLETE");
}

//Event onVolumeChangeCallback
// -----------------------------------------------------
V10Core.Player.prototype.onVolumeChangeCallback = function(volume){
    switch (this.options.core.api){
        case V10Core.Player.apis.HTML5:
            if (this.isMuted != this.player[0].muted) {
                this.isMuted = this.player[0].muted;

                if (this.isMuted) {
                    if (this.options.events.MUTE != null) this.options.events.MUTE(this);
                    this.debugLog("HTML5Video :: " + this.options.core.name + " :: MUTE", volume);
                } else {
                    if (this.options.events.UNMUTE != null) this.options.events.UNMUTE(this);
                    this.debugLog("HTML5Video :: " + this.options.core.name + " :: UNMUTE", volume);
                }
            } else {
                if (this.options.events.VOLUME_CHANGE != null) this.options.events.VOLUME_CHANGE(this);
                this.debugLog("HTML5Video :: " + this.options.core.name + " :: VOLUME_CHANGE", volume);
            }
            break;
        case V10Core.Player.apis.RC:
            if(this.options.events.VOLUME_CHANGE != null) this.options.events.VOLUME_CHANGE(this);
            this.debugLog("Player :: " + this.options.core.name + " :: VOLUME_CHANGE", volume);
            break;
        case V10Core.Player.apis.YOUTUBE:
            if(this.options.events.VOLUME_CHANGE != null) this.options.events.VOLUME_CHANGE(this);
            this.debugLog("Player :: " + this.options.core.name + " :: VOLUME_CHANGE", volume);
            break;
        case V10Core.Player.apis.VIMEO:
            if(this.options.events.VOLUME_CHANGE != null) this.options.events.VOLUME_CHANGE(this);
            this.debugLog("Player :: " + this.options.core.name + " :: VOLUME_CHANGE", volume);
            break;
    }
}

//Event onMuteCallback
// -----------------------------------------------------
V10Core.Player.prototype.onMuteCallback = function(){
    if(this.options.events.MUTE != null) this.options.events.MUTE(this);
    this.debugLog("Player :: " + this.options.core.name + " :: MUTE");
}

//Event onUnMuteCallback
// -----------------------------------------------------
V10Core.Player.prototype.onUnMuteCallback = function(){
    if(this.options.events.UNMUTE != null) this.options.events.UNMUTE(this);
    this.debugLog("Player :: " + this.options.core.name + " :: UNMUTE");
}

//Event onEnterFullScreenCallback
// -----------------------------------------------------
V10Core.Player.prototype.onEnterFullScreenCallback = function(){
    if(this.options.events.ENTER_FULL_SCREEN != null) this.options.events.ENTER_FULL_SCREEN(this);
    this.debugLog("Player :: " + this.options.core.name + " :: ENTER_FULL_SCREEN");
}

//Event onExitFullScreenCallback
// -----------------------------------------------------
V10Core.Player.prototype.onExitFullScreenCallback = function(){
    if(this.options.events.EXIT_FULL_SCREEN != null) this.options.events.EXIT_FULL_SCREEN(this);
    this.debugLog("Player :: " + this.options.core.name + " :: EXIT_FULL_SCREEN");
}

//Event onErrorCallback
// -----------------------------------------------------
V10Core.Player.prototype.onErrorCallback = function(){
    if(this.options.events.ERROR != null) this.options.events.ERROR(this);
    this.debugLog("Player :: " + this.options.core.name + " :: ERROR");
}


//Event onMetaLoadedCallback
// -----------------------------------------------------
V10Core.Player.prototype.onMetaLoadedCallback = function(metas){
    var _this = this;

    if(this.options.events.META_LOADED != null) this.options.events.META_LOADED(this);
    this.debugLog("Player :: " + this.options.core.name + " :: META_LOADED", metas);

    if(this.options.core.api == V10Core.Player.apis.HTML5){
        var forceMute = this.player.attr('muted');
        var forcePlay = this.player.attr('autoplay');

        setTimeout(function(){
            if(forceMute == 'true' && _this.player[0].muted == false){
                _this.player[0].muted = true;
            }

            if(forcePlay == 'autoplay' && _this.player[0].paused == true){
                _this.player[0].play();
            }
        }, 1000);
    }
}

//UI Event onUIEventPlay
// -----------------------------------------------------
V10Core.Player.prototype.onUIEventPlay = function(e){
    if(e != null) e.preventDefault();

    if(this.options.events.UI_PLAY != null) this.options.events.UI_PLAY(this);

    //Init the player by mod selected
    if(this.options.mod == V10Core.Player.mods.DEFAULT){
        if(this.ui.poster) this.ui.poster.hide();
        if(this.ui.btnPlay) this.ui.btnPlay.hide();
    }

    this.initPlayerByMod(e);
}

//Method global pause
// -----------------------------------------------------
// pause all players
V10Core.Player.pause = function(){
    for(var i = 0; i < V10Core.Player.count; i++){
        if(V10Core.Player.players[i].options.core.api != V10Core.Player.apis.IFRAME){
            V10Core.Player.players[i].pause();
        }
    }
}

//Method global stop
// -----------------------------------------------------
// stop all players
V10Core.Player.stop = function(){
    for(var i = 0; i < V10Core.Player.count; i++){
        if(V10Core.Player.players[i].options.core.api != V10Core.Player.apis.IFRAME){
            V10Core.Player.players[i].stop();
        }
    }
}

//Event bugFix
// -----------------------------------------------------
V10Core.Player.prototype.bugFix = function(){
    $(this.options.core.container).find("button:not([type])").attr("type", "button");
}

//Function debugLog
// -----------------------------------------------------
// Debug log utils
V10Core.Player.initPlayerByAttr = function(attr){
    var attrList = $("["+attr+"]:not([v10core-player-uAttr])").toArray();

    for(var i = 0; i < attrList.length; i++){

        V10Core.Player.attrCount++;
        var uContainerID = "v10core-player-uAttr";
        $(attrList[i]).attr(uContainerID,(V10Core.Player.count+1));
        var options = JSON.parse($(attrList[i]).attr(attr));
        if(!options.core) options.core = {};
        options.core.container = "["+uContainerID+"="+(V10Core.Player.count+1)+"]";

        var newPlayer = new V10Core.Player(options);
    }
}

//Function debugLog
// -----------------------------------------------------
// Debug log utils
V10Core.Player.prototype.debugLog = function(){
    if(this.options.core.debugMode){
        console.log(arguments);
    }
}

V10Core.Player.players = [];
V10Core.Player.count = 0;
V10Core.Player.playerAttribute = 'data-v10core-player';
V10Core.Player.mods = {DEFAULT:"default",ABSOLUTE:"absolute",FANCY_BOX:"fancybox",FIXED:"fixed"};
V10Core.Player.apis = {HTML5:"html5",RC:"rc",YOUTUBE:"youtube",VIMEO:"vimeo", IFRAME:"iframe",BRIGHTCOVE:"brightcove",TQ:"tq"};
V10Core.Player.YouTubeIframeAPIReady = false;
V10Core.Player.version = "4.0.1";

jQuery(document).ready(function(e){
    V10Core.Player.initPlayerByAttr(V10Core.Player.playerAttribute);
});

function onYouTubeIframeAPIReady() {
    V10Core.Player.YouTubeIframeAPIReady = true;
}
